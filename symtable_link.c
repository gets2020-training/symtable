#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "symtable.h"

struct PointerBindng{
    char *key;
    void *value;
    binding *next;
};

struct SymTable{
    binding *head;
    int length;
};

SymTable_t SymTable_new (void){
    SymTable_t new_table = (SymTable_t)malloc(sizeof(struct SymTable));
    if(new_table == NULL)
        return NULL;
    new_table->head = NULL;
    new_table->length = 0;
    return new_table;
}

void SymTable_free (SymTable_t oSymTable){
    if(oSymTable == NULL)
        return;
    binding *ptr;
    binding *cur;
    ptr = oSymTable->head;
    while(ptr != NULL){
        cur = ptr;
        ptr = ptr->next;
        free(cur);
    }
    free(oSymTable);
}
int SymTable_getLength (SymTable_t oSymTable){
    if(oSymTable==NULL){
        return 0;
    }
    return oSymTable->length;
}
int SymTable_put (SymTable_t oSymTable,const char *pcKey,const void *pvValue){
    if(oSymTable==NULL || pcKey ==NULL)
        return 0;
    if(SymTable_contains(oSymTable,pcKey))
        return 0;
    binding *new_node = (binding *)malloc(sizeof(binding));
    if(new_node == NULL)
        return 0;
    new_node->key=(char *) pcKey;
    new_node->value = (void *)pvValue;
    new_node->next = oSymTable->head;
    oSymTable->head = new_node;
    oSymTable->length++;
    return 1;
}
void *SymTable_replace (SymTable_t oSymTable,const char *pcKey,const void *pvValue){
    if(oSymTable == NULL || pcKey ==NULL)
        return NULL;
    binding *ptr;
    ptr = oSymTable->head;
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            void *old_value=ptr->value;
            ptr->value=(void *)pvValue;
            return old_value;
        }
        ptr = ptr->next;
    }
    return NULL;
}




int SymTable_contains (SymTable_t oSymTable,const char *pcKey){
    if(oSymTable == NULL || pcKey ==NULL)
        return 0;
    binding *ptr;
    ptr = oSymTable->head;
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            return 1;
        }
        ptr = ptr->next;
    }
    return 0;
}



void *SymTable_get (SymTable_t oSymTable, const char *pcKey){
    if(oSymTable == NULL || pcKey ==NULL)
        return NULL;
    binding *ptr;
    ptr = oSymTable->head;
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            return ptr->value;
        }
        ptr = ptr->next;
    }
    return NULL;
}



void *SymTable_remove (SymTable_t oSymTable,const char *pcKey){
    if(oSymTable == NULL || pcKey ==NULL)
        return NULL;
    binding *ptr;
    binding *prev=NULL;
    ptr = oSymTable->head;
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            void *old_value=ptr->value;
            if(prev==NULL){
                oSymTable->head=ptr->next;
                free(ptr);
            }
            else{
                prev->next = ptr->next;
                free(ptr);
            }
            oSymTable->length--;
            return old_value;
        }
        prev=ptr;
        ptr = ptr->next;
    }
    return NULL;
}



void SymTable_map (SymTable_t oSymTable,void (*pfApply) (const char *pcKey,const void *pvValue,void *pvExtra),const void *pvExtra){
    if(oSymTable == NULL || pfApply == NULL)
        return ;
    binding *ptr;
    ptr = oSymTable->head;
    while(ptr != NULL){
        (*pfApply)(ptr->key,ptr->value,(void *)pvExtra);
        ptr=ptr->next;
    }
    return ;
}




