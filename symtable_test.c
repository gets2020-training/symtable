#include <stdio.h>
#include <string.h>

#include "symtable.h"

void Symtable_printing(const char *pcKey,const void *pvValue,void *pvExtra){
    printf("Map %s %s\n",pcKey,(char *)pvValue);
}

int main(){
    
    SymTable_t table = SymTable_new();
    char *pcKey = "Key1";
    void *pvValue = "value1";
    printf("%d\n",SymTable_getLength(table));    
    int check = SymTable_put (table,pcKey,pvValue);
    pvValue = "value2";
    printf("%d",check);
    
    void (*fp)(const char *pcKey,const void *pvValue,void *pvExtra)=Symtable_printing;
    SymTable_map(table,(*fp),"");
    check = SymTable_put (table,pcKey,pvValue);
    pcKey = "Key2";
    pvValue = "value2";
    check = SymTable_put (table,pcKey,pvValue);
    pcKey = "Key3";
    pvValue = "Value3";
    check = SymTable_put (table,pcKey,pvValue);
    pcKey = "Key4";
    pvValue = "Value4";



    check = SymTable_put (table,pcKey,pvValue);
    SymTable_print(table);
    printf("%s\n",(char *)SymTable_get(table,"Key1"));
    printf("%s\n",(char *)SymTable_get(table,"Key2"));
    printf("%s\n",(char *)SymTable_get(table,"Key3"));
    SymTable_replace(table,"HI",(void *)"Value1");
    printf("%s\n",(char *)SymTable_get(table,"Key2"));
    printf("%d ",SymTable_contains (table,pcKey));
    printf("%s \n===\n",(char *)SymTable_get (table,pcKey));

    //void (*fp)(const char *pcKey,const void *pvValue,void *pvExtra)=Symtable_printing;
    SymTable_map(table,(*fp),"");

    printf("%s \n===\n",(char *)SymTable_remove(table,"HI"));
    printf("\n");
    SymTable_remove(table,"Key2");
    SymTable_remove(table,"Key3");
    

    SymTable_free (table);
    return 0;
}