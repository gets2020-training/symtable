#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "symtable.h"
#include "hash.h"

static const unsigned int BucketCounts[] ={509, 1021, 2039, 4093, 8191, 16381, 32749, 65521};

struct PointerBindng{
    char *key;
    void *value;
    binding *next;
};

struct SymTable{
    binding **bucket;
    unsigned int bucketSize;
    unsigned int bindingCount;
};

SymTable_t SymTable_new (void){
    SymTable_t new_table;
    new_table = (SymTable_t)malloc(sizeof(struct SymTable));
    if(new_table == NULL)
        return NULL;
    new_table->bucketSize=BucketCounts[0];
    new_table->bindingCount=0;
    new_table->bucket = (binding**)calloc(new_table->bucketSize,sizeof(binding*));
    if(new_table->bucket == NULL){
        free(new_table);
        return NULL;
    }
    return new_table;
}
void SymTable_free (SymTable_t oSymTable){
    if(oSymTable == NULL)
        return;
    binding *ptr;
    binding *cur;
    for(unsigned int idx = 0 ; idx < oSymTable->bucketSize ; idx++){
        ptr = (oSymTable->bucket)[idx];
        while(ptr != NULL){
            cur=ptr;
            ptr=ptr->next;
            free(cur);
        }
    }
    free(oSymTable->bucket);
    free(oSymTable);
}
int SymTable_getLength (SymTable_t oSymTable){
    if(oSymTable==NULL)
        return 0;
    return oSymTable->bindingCount;
}
static SymTable_expand(SymTable_t oSymTable){
    unsigned int newBucketIdx = 0;
    while(BucketCounts[newBucketIdx] != oSymTable->bucketSize){
        newBucketIdx++;
    }
    unsigned int newBucketSize = BucketCounts[newBucketIdx+1];
    binding **newBucket = (binding**)calloc(newBucketSize,sizeof(binding*));
    if(newBucket == NULL)
        return;
    binding *ptr;
    binding *nextPtr=NULL;
    for(unsigned int idx = 0;idx < oSymTable->bucketSize; idx++){
        ptr = (oSymTable->bucket)[idx];
        while(ptr != NULL){
            nextPtr = ptr->next;
            newBucketIdx = SymTable_hash(ptr->key,newBucketSize);
            ptr->next = newBucket[newBucketIdx];
            newBucket[newBucketIdx]=ptr;
            ptr=nextPtr;
        }
    }
    free(oSymTable->bucket);
    oSymTable->bucket = newBucket;
    oSymTable->bucketSize = newBucketSize;
    return;
}
int SymTable_put (SymTable_t oSymTable,const char *pcKey,const void *pvValue){
    if(oSymTable == NULL || pcKey ==NULL)
        return 0;
    if(SymTable_contains(oSymTable,pcKey))
        return 0;
    binding *new_node = (binding *)malloc(sizeof(binding));
    if(new_node == NULL)
        return 0;
    new_node->key=(char *) pcKey;
    new_node->value = (void *)pvValue;
    if((oSymTable->bindingCount > oSymTable->bucketSize) && (oSymTable->bucketSize != BucketCounts[sizeof(BucketCounts)/sizeof(unsigned int)-1])){
printf("ghfhi");
        (void)SymTable_expand(oSymTable);
    }
    unsigned int bucketIdx = SymTable_hash(pcKey,oSymTable->bucketSize);
    new_node->next = (oSymTable->bucket)[bucketIdx];
    (oSymTable->bucket)[bucketIdx] = new_node;
    oSymTable->bindingCount++;
    return 1;
}
void *SymTable_replace (SymTable_t oSymTable,const char *pcKey,const void *pvValue){
    if(oSymTable == NULL || pcKey ==NULL)
        return NULL;
    binding *ptr;
    unsigned int bucketIdx = SymTable_hash(pcKey,oSymTable->bucketSize);
    ptr = (oSymTable->bucket)[bucketIdx];
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            void *old_value=ptr->value;
            ptr->value=(void *)pvValue;
            return old_value;
        }
        ptr=ptr->next;
    }
    return NULL;
}
int SymTable_contains (SymTable_t oSymTable,const char *pcKey){
    if(oSymTable == NULL || pcKey ==NULL)
        return 0;
    binding *ptr;
    unsigned int bucketIdx = SymTable_hash(pcKey,oSymTable->bucketSize);
    ptr = (oSymTable->bucket)[bucketIdx];
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            return 1;
        }
        ptr=ptr->next;
    }
    return 0;
}
void *SymTable_get (SymTable_t oSymTable, const char *pcKey){
    if(oSymTable == NULL || pcKey ==NULL)
        return NULL;
    binding *ptr;
    unsigned int bucketIdx = SymTable_hash(pcKey,oSymTable->bucketSize);
    ptr = (oSymTable->bucket)[bucketIdx];
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            return ptr->value;
        }
        ptr=ptr->next;
    }
    return NULL;
}
void *SymTable_remove (SymTable_t oSymTable,const char *pcKey){
    if(oSymTable == NULL || pcKey ==NULL)
        return NULL;
    binding *ptr;
    binding *prev=NULL;
    unsigned int bucketIdx = SymTable_hash(pcKey,oSymTable->bucketSize);
    ptr = (oSymTable->bucket)[bucketIdx];
    while(ptr != NULL){
        if(strcmp(ptr->key,pcKey)==0){
            void *old_value = ptr->value;
            if(prev==NULL){
                (oSymTable->bucket)[bucketIdx]=ptr->next;
                free(ptr);
            }
            else{
                prev->next=ptr->next;
                free(ptr);
            }
            oSymTable->bindingCount--;
            return old_value;
        }
        prev=ptr;
        ptr=ptr->next;
    }
    return NULL;
}
void SymTable_map (SymTable_t oSymTable,void (*pfApply) (const char *pcKey,const void *pvValue,void *pvExtra),const void *pvExtra){
    if(oSymTable == NULL || pfApply == NULL)
        return ;
    binding *ptr;
    for(unsigned int idx = 0 ; idx < oSymTable->bucketSize ; idx++){
        ptr = (oSymTable->bucket)[idx];
        while(ptr != NULL){
            (*pfApply)(ptr->key,ptr->value,(void *)pvExtra);
            ptr=ptr->next;
        }
    }
    return;
}
